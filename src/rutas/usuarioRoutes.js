const { Router } = require('express');
const router = Router();
const usuarioController = require('../controllers/usuarioController');
const isAuth = require('../middlware/is-auth');

router.post('/verificar', usuarioController.isAuth);
router.post('/register', usuarioController.register);
router.post('/', usuarioController.logIn);
router.post('/logout', usuarioController.logOut);


module.exports = router;