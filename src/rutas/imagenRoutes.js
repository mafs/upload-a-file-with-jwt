const { Router } = require('express');
const router = Router();
const upload = require('../utilidades/subirImagen');
const imagenController = require('../controllers/imagenController');
const isAuth = require('../middlware/is-auth');
// const upload = require('multer')({dest: 'src/uploads'});
router.use(isAuth);
// para subir imagenes debes de estar logeado por lo tanto tener un token valido
router.post('/', [upload.single('img')], (req, res) => {
    res.end('se guardo correctament el imagen');
});
router.put('/update/:id', [upload.single('imagen')], imagenController.updateImg);

module.exports = router;

// eyJhbGciOiJFUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7Il9pZCI6IjVkOWU1NzJiOTg0ZTQzMjFjYzYyZTU2OSIsInVzdWFyaW8iOiJtYWZzcyIsInBhc3N3b3JkIjoiJDJiJDEwJFhocTdZcm5QMUVmYjVnOUNKTjVOVy5CTlZDWkFiQ01aZTY1Rnh6VGQ5SEJ6bGJZTm9tMnZPIiwiX192IjowfSwiaWF0IjoxNTcwNjU4MTIwLCJleHAiOjE1NzA2OTQxMjB9.ANmdHJ7Yufsc7puQzozRGiQ4AqLvHTaJLSO1hljkOWmqEzvt55YSBuFx345G6z4As7pUx4GoshrsH1_vOFXFqCwyAII9VYCKgYLKkp6Xu20Yz0sQD5bYwNswQvfuLW1aDoRWDuFm6IGkE3OjqIW2sadKahudB9J46dcb8VamBru13611