const { Schema, model } = require('mongoose');


const usuarioSchema = Schema({
    usuario: String,
    password: String
});


module.exports = model('usuarios', usuarioSchema);