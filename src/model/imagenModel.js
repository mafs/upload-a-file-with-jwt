const { Schema, model } = require('mongoose');

const imagenSchema = Schema({
  imagen : { type: String }
});

module.exports = model('imagenes', imagenSchema);