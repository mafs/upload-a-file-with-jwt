const multer = require('multer');
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './src/img');
  },
  filename: (req, file, cb) => {
    const arreglo = file.originalname.split('.');
    const ext = arreglo[arreglo.length-1];
    cb(null, `${file.fieldname}-mafs-${Date.now()}.${ext}`);
  }
});

const upload = multer({ storage });
module.exports = upload;