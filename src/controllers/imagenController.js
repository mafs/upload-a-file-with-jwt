const fs = require('fs');
const Imagen = require('../model/imagenModel');
const path = require('path');

const imagenController = {
    saveImg: async (req, res) => {
        const img = new Imagen({
            imagen: req.file.filename
        });
        try {
            const imgenGuardada = await img.save();
            res.json({img:imgenGuardada});
        } catch (error) {
            res.status(500).json({error});
        }
    },
    updateImg: (req, res) => {
        const { id } = req.params;
        verificarImg(id).then( async data => {
            console.log(data);
            const img = new Imagen({
                imagen: req.file.filename
            });
            try {
                const imgenGuardada = await img.save();
                return res.json({img:imgenGuardada});
            } catch (error) {
                return res.status(500).json({error});
            }
        }).catch((err) => {
            console.log(err);
            return res.status(500).json({error, err});
        });
    },
    deleteImg: (req, res) => {
    
    }
};

function verificarImg(id) {
    return new Promise((resolve, reject) => {
        Imagen.findById(id).then(imagen => {
            if(fs.existsSync(path.join(__dirname, `../img/${imagen.imagen}`))) {
                fs.unlink(path.join(__dirname, `../img/${imagen.imagen}`), err => {
                    console.log(err);
                    if(err) return reject('ocurrio un error, al intentar borrar imagen' + err);

                    resolve(`el archivo fue borrado`);
                });
            }
        });
    });
}
module.exports = imagenController;