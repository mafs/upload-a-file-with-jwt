const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const path = require('path');
const Usuario = require('../model/usuarioModel');

const usuarioController = {
    logIn: (req, res) => {
        const { password, usuario } = req.body;
        if(!password || !usuario) return res.status(400).json({msn:'el usuario o password no son validos'});
        Usuario.findOne({usuario})
            .then( async user => {
                if(!user) return res.status(401).json({msn: 'usuario no existe'});
                const match = await bcrypt.compare(password, user.password);
                if(!match) return res.status(500).json({msn:'el usuario o la contraseña esta mal - PASSWORD'});

                const privateKey = fs.readFileSync(path.join( __dirname, '../ecdsa-p521-private.key' ));
                    try {
                        const token = await jwt.sign({ user }, privateKey, { algorithm: 'ES512', expiresIn : '10h'});
                        res.json({token});
                    } catch (error) {
                        return res.status(500).json({msn:'ocurrio un error al hacer el token'});
                    }
            })
            .catch(error => res.status(500).json({error}));
    },
    logOut: (req, res) => {
        console.log('se salio');
        res.send('se salio de la app');
    },
    register: async (req, res) => {
        const { usuario, password } = req.body;
        if( !usuario || !password ) return res.status(400).json({msn:'no es valido el password o el usuario'});
        console.log(usuario, password);
        try {
            const passwordHash = await bcrypt.hash(password, 10);
            const usuarioSave = new Usuario({
                usuario,
                password: passwordHash
            });
            const usuarioSaved = await usuarioSave.save();
            return res.json({msn:'usuario guardado', usuario:usuarioSaved});
        } catch (error) {
            return res.status(400).json({error});
        }
    },
    isAuth: (req, res) => {
        console.log(req.usuario);
        res.json({usuario: req.usuario});
    }
};

module.exports = usuarioController;