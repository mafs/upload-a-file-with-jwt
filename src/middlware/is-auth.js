const jwt = require('jsonwebtoken');
const fs = require('fs');
const path = require('path');
const cert = fs.readFileSync( path.join(__dirname, '../ecdsa-p521-public.key'));  // get public key
isAuth = (req, res, next) => {
    const { token } = req.headers;
    try {
        const decoded = jwt.verify(token, cert);
        req.usuario = decoded;
        next();
      } catch(err) {
        res.status(400).json({msn:'el token no es valido o no estas registrado'});
      }
};

module.exports = isAuth;