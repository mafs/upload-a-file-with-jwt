const express = require('express');
const app = express();
require('./DB/conexion');
const morgan = require('morgan');
app.use( express.json() );
app.use( express.urlencoded({extended: false, limit: 100000000}));
app.use(morgan('dev'));

app.use('/img', require('./rutas/imagenRoutes'));
app.use('/user', require('./rutas/usuarioRoutes'));

app.listen(3000, () => console.log('server on port 3000'));