const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/imagen', {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
)
  .then( () => console.log('se conecto a la BD de mongo'))
  .catch((err) => console.log('ocurrio un error al conectar a la BD: ', err));